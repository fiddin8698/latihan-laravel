<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        //dd($request->all());
        $namadepan = $request->nama1;
        $namabelakang = $request->nama2;

        return view('halaman.home', compact('namadepan', 'namabelakang'));
    }
}