@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
        <h4> Sign Up Form </h4>
        <form action="welcome" method="post">
            @csrf
            <!--nama depan-->
            <label> First Name: </label> <br>
            <input type="text" name="nama1">
            <br><br>
            <!--nama belakang-->
            <label> Last Name: </label> <br>
            <input type="text" name="nama2">
            <br><br>
            <!--jenis kelamin-->
            <label> Gender: </label> <br>
            <input type="radio" name="gender" value="Male" > Male      <br>
            <input type="radio" name="gender" value="Female" > Female    <br>
            <input type="radio" name="gender" value="Other" > Other
            <br><br>
            <!--kewarganegaraan-->
            <label> Nationality: </label> <br>
            <select>
                <option value="Indonesia"> Indonesia </option>    <br>
                <option value="Amerika"> Amerika </option>      <br>
                <option value="Inggris"> Inggris </option>      <br>
            </select>
            <br><br>
            <!--bahasa sehari-hari-->
            <label> Language Spoken: </label> <br>
            <input type="checkbox" name="language spoken" value="Indonesia" > Bahasa Indonesia   <br>
            <input type="checkbox" name="language spoken" value="English" > English            <br>
            <input type="checkbox" name="language spoken" value="Other" > Other
            <br><br>
            <!--biografi singkat-->
            <label> Bio : </label> <br>
            <textarea  name="bio" cols="30" rows="10"></textarea>
            <br><br>
            <!--submit-->
            <a href="welcome.html" > <input type="submit" value="kirim">
        </form>
@endsection