@extends('layout.master')

@section('judul')
<h4> Media Online </h4>
@endsection

@section('content')
<div>
    <h5> Sosial Media Developer </h5>
    <p> Belajar dan Berbagi agar hidup lebih baik </p>
 </div>
 <!--benefit join di media online-->
 <div>
    <h5> Benefit Join di Media Online </h5>
    <ul>
        <li> Mendapatkan motivasi dari para sesama developer </li>
        <li> Sharing knowledge </li>
        <li> Dibuat oleh calon web developer terbaik</li>
    </ul>
 </div>
 <!--cara bergabung ke media online-->
 <div>
    <h5> Cara Bergabung ke Media Online </h5>
    <ol>
        <li> Mengunjungi website ini </li>
        <li> Mendaftarkan di <a href="/form" > Form Sign Up</a></li>
        <li> Selesai </li>
    </ol>
 </div>
@endsection