<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'DashboardController@index');

Route::get('/form', 'FormController@bio');

Route::post('/welcome', 'FormController@kirim');

Route::get('/data-table', function(){
    return view('tables.data-table');
});

Route::get('/table', function(){
    return view('tables.table');
});

//CRUD Cash
Route::get('/cash/create', 'CashController@create');
Route::post('/cash', 'CashController@store');
Route::get('/cash', 'CashController@index');
Route::get('/cash/{cash_id}', 'CashController@show');
Route::get('/cash/{cash_id}/edit', 'CashController@edit');
Route::put('/cash/{cash_id}', 'CashController@update');
Route::delete('/cash/{cash_id}', 'CashController@destroy');